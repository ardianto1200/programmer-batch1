<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Employe
Route::get('/FVemploye', 'employeController@index');

Route::get('/FVemploye/create', 'employeController@create');
Route::post('/FVemploye', 'employeController@store');

Route::get('/FVemploye/{id}', 'employeController@show');

Route::get('/FVemploye/{id}/edit', 'employeController@edit');
Route::put('/FVemploye/{id}', 'employeController@update');

Route::delete('/FVemploye/{id}', 'employeController@destroy');

//Company
Route::get('/FVcompany', 'companyController@index');

Route::get('/FVcompany/create', 'companyController@create');
Route::post('/FVcompany', 'companyController@store');

Route::get('/FVcompany/{id}', 'companyController@show');

Route::get('/FVcompany/{id}/edit', 'companyController@edit');
Route::put('/FVcompany/{id}', 'companyController@update');

Route::delete('/FVcompany/{id}', 'companyController@destroy');
