<?php

namespace App\Http\Controllers;

use App\companyModel;
use Illuminate\Http\Request;

class companyController extends Controller
{
    public function index()
    {
        $company = companyModel::all();
        return view('FVcompany/homeCompany', ['company'=>$company]);
    }
    public function show($id)
    {
        $company = companyModel::find($id);
        if (!$company)
            abort(404);
        return view('FVcompany/single', ['company'=>$company]);
    }

    public function create(){
        return view('FVcompany/create');

    }

    public function store(Request $request){
        $company = new companyModel;
        $company->id = $request->id;
        $company->nama = $request->nama;
        $company->alamat = $request->alamat;
        $company->save();
        return redirect('FVcompany');
    }

    public function edit($id){
        $company = companyModel::find($id);
        return view('FVcompany/edit', ['company'=>$company]);
    }

    public function update(Request $request, $id){
        $company = companyModel::find($id);
        $company->id = $request->id;
        $company->nama = $request->nama;
        $company->alamat = $request->alamat;
        $company->save();
        return redirect('FVcompany/'. $id);

    }
    public function destroy($id){
        $company = companyModel::find($id);
        $company->delete();
        return redirect('FVcompany');
    }
}
