<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\EmployeeModel;
use Illuminate\Support\Facades\DB;


class employeController extends Controller
{
    public function index()
    {
        $employee = EmployeeModel::all();
        return view('FVemploye/homeEmploye', ['employee'=>$employee]);
    }
    public function show($id)
    {
        $employee = EmployeeModel::find($id);
        if (!$employee)
            abort(404);
        return view('FVemploye/single', ['employee'=>$employee]);
    }

    public function create(){
        return view('FVemploye/create');

    }

    public function store(Request $request){
        $employee = new EmployeeModel;
        $employee->id = $request->id;
        $employee->nama = $request->nama;
        $employee->atasan_id = $request->atasan_id;
        $employee->company_id = $request->company_id;
        $employee->save();
        return redirect('FVemploye');
    }

    public function edit($id){
        $employee = EmployeeModel::find($id);
        return view('FVemploye/edit', ['employee'=>$employee]);
    }

    public function update(Request $request, $id){
        $employee = EmployeeModel::find($id);
        $employee->id = $request->id;
        $employee->nama = $request->nama;
        $employee->atasan_id = $request->atasan_id;
        $employee->company_id = $request->company_id;
        $employee->save();
        return redirect('FVemploye/'. $id);

    }
    public function destroy($id){
        $employe = EmployeeModel::find($id);
        $employe->delete();
        return redirect('FVemploye');
    }
}
