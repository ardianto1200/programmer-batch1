<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class companyModel extends Model
{
    protected $table = 'company';
    public $timestamps = false; //Tidak ada created dan update_at
    protected $fillable = ['nama', 'alamat'];
}
