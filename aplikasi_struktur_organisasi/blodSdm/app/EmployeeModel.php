<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    protected $table = 'employee';
    public $timestamps = false; //Tidak ada created dan update_at
    protected $fillable = ['nama', 'atasan_id', 'company_id'];
}
