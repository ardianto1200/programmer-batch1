<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
</head>
<body>
<header>
    <nav>
        <a href="/">Home</a>
        <a href="/FVemploye">Employee</a>
        <a href="/FVcompany">Company</a>
    </nav>
</header>

@yield('content')

<footer>
    <p><hr> &copy; Ardianto 2020 </p>
</footer>
</body>
</html>
