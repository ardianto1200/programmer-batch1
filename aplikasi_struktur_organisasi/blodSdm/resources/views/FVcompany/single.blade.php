@extends('layouts.master')

@section('title','Detail')

@section('content')
    <h1>Data Edit</h1>
    <table border="1">
        <tr>
            <th>ID Perusahaan</th>
            <th>Nama Perusahaan</th>
            <th>Alamat Perusahaan</th>
        </tr>
        <tr>
            <td>{{$company->id}}</td>
            <td>{{$company->nama}}</td>
            <td>{{$company->alamat}}</td>
    </table>
@endsection
