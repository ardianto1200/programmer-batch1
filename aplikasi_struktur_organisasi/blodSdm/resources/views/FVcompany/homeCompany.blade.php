@extends('layouts.master')

@section('title','Employee')

@section('content')
    <div class="content">
        <h1>Data Lengkap Company</h1>
        <hr>
        <form action="/FVcompany/create">
            <input type="submit" name="submit" value="Tambah">
        </form><br>
        @foreach ($company as $company)
            <table border="1">
                <tr>
                    <th>ID Company</th>
                    <th>Nama Perusahaan</th>
                    <th>Alamat Perusahaan</th>
                    <th colspan="2">Aksi</th>
                </tr>
                <tr>
                    <td>{{$company->id}}</td>
                    <td>{{$company->nama}}</td>
                    <td>{{$company->alamat}}</td>
                    <td>
                        <form action="/FVcompany/{{$company->id}}/edit">
                            <input type="submit" name="submit" value="edit">
                        </form>
                    </td>
                    <td>
                        <form action="/FVcompany/{{$company->id}}" method="post">
                            <input type="submit" name="submit" value="hapus">
                            {{csrf_field()}}
                            <input type="hidden" name= "_method" value ="DELETE">
                        </form>
                    </td>
                </tr>
            </table>
    @endforeach
@endsection
