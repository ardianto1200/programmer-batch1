@extends('layouts.master')

@section('title','Edit Data')

@section('content')
    <h1>Halaman Edit</h1>
    <form action="/FVcompany/{{$company->id}}" method="post">

        <input type="text" name="nama" value = "{{$company->nama}}"><br><br>
        <input type="text" name="alamat" value="{{$company->alamat}}"><br><br>
        <input type="submit" name="submit" value="Edit">
        {{csrf_field()}}
        <input type="hidden" name= "_method" value ="PUT">
    </form>

@endsection
