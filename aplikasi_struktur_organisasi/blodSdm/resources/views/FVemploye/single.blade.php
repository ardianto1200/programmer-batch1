@extends('layouts.master')

@section('title','Detail')

@section('content')
    <h1>Data Edit</h1>
    <table border="1">
        <tr>
            <th>ID Employee</th>
            <th>Nama Lengkap</th>
            <th>Atasan ID</th>
            <th>Company ID</th>
        </tr>
        <tr>
            <td>{{$employee->id}}</td>
            <td>{{$employee->nama}}</td>
            <td>{{$employee->atasan_id}}</td>
            <td>{{$employee->company_id}}</td>
    </table>
@endsection
