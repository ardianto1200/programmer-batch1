@extends('layouts.master')

@section('title','Edit Data')

@section('content')
    <h1>Halaman Edit</h1>
    <form action="/FVemploye/{{$employee->id}}" method="post">

        <input type="text" name="nama" value = "{{$employee->nama}}"><br><br>
        <input type="text" name="atasan_id" value="{{$employee->atasan_id}}"><br><br>
        <input type="text" name="company_id" value="{{$employee->company_id}}"><br><br>
        <input type="submit" name="submit" value="Edit">
        {{csrf_field()}}
        <input type="hidden" name= "_method" value ="PUT">
    </form>

@endsection
