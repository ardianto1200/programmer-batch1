
@extends('layouts.master')

@section('title','Tambah Data')

@section('content')
    <h1>Halaman Create</h1>
    <form action="/FVemploye" method="post">

        <input type="text" name="nama" placeholder="Masukan Nama Lengkap"><br><br>
        <input type="text" name="atasan_id" placeholder="Masukan Atasan ID"><br><br>
        <input type="text" name="company_id" placeholder="Masukan Company ID"><br><br>
        <input type="submit" name="submit" value="Create">
        {{csrf_field()}}

    </form>

@endsection
