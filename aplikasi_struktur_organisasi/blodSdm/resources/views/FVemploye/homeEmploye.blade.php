@extends('layouts.master')

@section('title','Employee')

@section('content')
    <div class="content">
    <h1>Data Lengkap Employee</h1>
    <hr>
    <form action="/FVemploye/create">
        <input type="submit" name="submit" value="Tambah">
    </form><br>
    @foreach ($employee as $employee)
        <table border="1">
            <tr>
                <th>ID Employee</th>
                <th>Nama Lengkap</th>
                <th>Atasan ID</th>
                <th>Company ID</th>
                <th colspan="2">Aksi</th>
            </tr>
                <tr>
                    <td>{{$employee->id}}</td>
                    <td>{{$employee->nama}}</td>
                    <td>{{$employee->atasan_id}}</td>
                    <td>{{$employee->company_id}}</td>
                    <td>
                        <form action="/FVemploye/{{$employee->id}}/edit">
                            <input type="submit" name="submit" value="edit">
                        </form>
                    </td>
                    <td>
                        <form action="/FVemploye/{{$employee->id}}" method="post">
                            <input type="submit" name="submit" value="hapus">
                            {{csrf_field()}}
                            <input type="hidden" name= "_method" value ="DELETE">
                        </form>
                    </td>
                </tr>
        </table>
    @endforeach
@endsection
