-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 10, 2020 at 03:57 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE `departemen` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`id`, `nama`) VALUES
(1, 'Manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'Teknisi'),
(4, 'Analis');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis kelamin` varchar(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `tanggal lahir` date NOT NULL,
  `tanggal masuk` date NOT NULL,
  `Departemen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `jenis kelamin`, `status`, `tanggal lahir`, `tanggal masuk`, `Departemen`) VALUES
(1, 'Rizki Saputra', 'L', 'Menikah', '1980-11-10', '2011-01-01', 1),
(2, 'Farhan Reza', 'L', 'Belum', '1989-01-11', '2011-01-01', 1),
(3, 'Riyando Adi', 'L', 'Menikah', '1977-01-25', '2011-01-01', 1),
(4, 'Diego Manuel', 'L', 'Menikah', '1983-02-22', '2012-04-09', 2),
(5, 'Satya Laksana', 'L', 'Menikah', '1981-01-12', '2011-03-19', 2),
(6, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', 2),
(7, 'Putri Persada', 'P', 'Menikah', '1988-01-30', '2013-04-14', 2),
(8, 'Alman Safira', 'P', 'Menikah', '1991-05-18', '2013-09-28', 3),
(9, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-03-09', 3),
(10, 'Abi Isyawara', 'L', 'Belum', '1991-06-03', '2012-01-22', 3),
(11, 'Maman Kresna', 'L', 'Belum', '1993-08-21', '2012-09-15', 3),
(12, 'Nadia Aulia', 'P', 'Belum', '1989-07-18', '2012-05-07', 4),
(13, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', 4),
(14, 'Dani Setiawan', 'L', 'Belum', '1986-02-11', '2014-11-30', 4),
(15, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-12-03', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departemen`
--
ALTER TABLE `departemen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
