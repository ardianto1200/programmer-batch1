CREATE TABLE `employee` (
  `id_employee` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `atasan_id` varchar(100) DEFAULT NULL,
  `company_id` varchar(100) DEFAULT NULL
);
insert into employee (id_employee, nama, atasan_id, company_id) VALUES
('1', 'Pak Budi',NULL,'1');
insert into employee (id_employee, nama, atasan_id, company_id) VALUES
('2', 'Pak Tono','1','1'),
('3', 'Pak Totok','1','1'),
('4', 'Bu Sinta','2','1'),
('5', 'Bu Novi','3','1'),
('6', 'Andre','4','1'),
('7', 'Dono','4','1'),
('8', 'Ismir','5','1'),
('9', 'Anto','5','1');

CREATE TABLE `company` (
  `id_company` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL
);
insert into company(id_company, nama, alamat) VALUES
('1','PT JAVAN','Sleman'),
('2','PT Dicoding','Bandung');

select * from employee where atasan_id is NULL;

select * from employee where atasan_id in ('4', '5');

select * from employee where atasan_id = '1';

select * from employee where atasan_id in ('2','3');

select  COUNT(1)
from    (select * from employee
         order by atasan_id, id_employee) employe_sorted,
        (select @emply := (SELECT id_employee FROM employee WHERE nama='Pak Budi')) initialisation
where   find_in_set(atasan_id, @emply)
and     length(@emply := concat(@emply, ',', id_employee));

select  COUNT(1)
from    (select * from employee
         order by atasan_id, id_employee) employe_sorted,
        (select @emply := (SELECT id_employee FROM employee WHERE nama='Bu Sinta')) initialisation
where   find_in_set(atasan_id, @emply)
and     length(@emply := concat(@emply, ',', id_employee));

